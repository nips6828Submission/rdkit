from ubuntu  as Rdkit
workdir /
run apt-get update -y
run apt-get install -y git
run apt-get install -y g++
run apt-get install -y libboost-all-dev
run apt-get install -y cmake
run apt-get install -y python3
run apt-get install -y python3-numpy
run git clone --recursive https://github.com/rdkit/rdkit.git
run mkdir rdkit-install
workdir /rdkit 
run mkdir build
workdir /rdkit/build
run cmake -DPYTHON_EXECUTABLE=/usr/bin/python3 -DRDK_INSTALL_INTREE=0 ..
run make -j$(nproc)
run make DESTDIR=/rdkit-install -j$(nproc) install 
copy rdkit-config.cmake /rdkit-install/usr/local/lib/cmake/rdkit/

from scratch 
Maintainer nips6828@gmail.com
workdir /
copy --from=Rdkit /rdkit-install /
